package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"golang.org/x/crypto/ssh"
)

type Connection struct {
	*ssh.Client
	password string
}

func isContain(timezone string) bool {
	arr := []string{"1", "2", "3", "4", "5", "6", "7", "8"}
	for _, w := range arr {
		if w == timezone {
			return true
		}
	}
	return false
}

func main() {

	var choice string
	fmt.Println("Выберите вариант: 1) Запись с sd card в нано 2) Подготовка sd карты")
	fmt.Scan(&choice)

	switch choice {
	case "1":
		write2nano()
	case "2":
		write2sd()
	default:
		return
	}

}
func write2nano() {
	var (
		ip string
		// pass     string
	)
	fmt.Print("Введите IP адрес нано: ")
	fmt.Scan(&ip)

	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
		}
	}()
	// Key is ssh private key
	signer, err := ssh.ParsePrivateKey([]byte(key))
	if err != nil {
		log.Fatalf("parse key failed:%v", err)
	}

	cfg := &ssh.ClientConfig{
		User:            "ch",
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	cfg.SetDefaults()

	conn, err := ssh.Dial("tcp", ip+":22", cfg)
	if err != nil {
		log.Fatal(err)
	}
	session, err := conn.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	session.Stdout = os.Stdout
	session.Stderr = os.Stderr
	stdin, err := session.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}
	defer stdin.Close()

	if err := session.Run("sudo rm -rf create_armbian.sh && sudo wget https://gitlab.com/new_fw_nano/new_nano_config_script/-/raw/dev/create_armbian.sh && sudo chmod +x create_armbian.sh && sudo ./create_armbian.sh force"); err != nil {
		log.Fatal(err)
	}

}

func write2sd() {
	var (
		ip       string
		login    string = "root"
		timezone string
	)
	fmt.Print("Введите IP адрес нано: ")
	fmt.Scan(&ip)

	fmt.Println("Выберите: 1.Astana 2.Almaty 3.Shymkent 4.Tashkent 5.Atyrau 6.Oral 7.Slave Nano 8.For Old Nano")
	fmt.Scan(&timezone)
	for !isContain(timezone) {
		fmt.Println("Введите 1, 2, 3, 4, 5, 6, 7 или 8")
		fmt.Scan(&timezone)
	}

	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
		}
	}()

	conn, err := Connect(ip+":22", login, "1234")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	session, err := conn.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	session.Stdout = os.Stdout
	session.Stderr = os.Stderr
	stdin, err := session.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}
	defer stdin.Close()
	go func() {
		defer stdin.Close()
		io.WriteString(stdin, timezone+"\n")
		io.WriteString(stdin, "10\n")
	}()

	if err := session.Run("wget https://gitlab.com/new_fw_nano/new_nano_config_script/-/raw/dev/install_new_firmware.sh && chmod +x install_new_firmware.sh && ./install_new_firmware.sh"); err != nil {
		log.Fatal(err)
	}
}

func Connect(addr, user, password string) (*Connection, error) {
	sshConfig := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.HostKeyCallback(func(hostname string, remote net.Addr, key ssh.PublicKey) error { return nil }),
	}
	conn, err := ssh.Dial("tcp", addr, sshConfig)
	if err != nil {
		return nil, err
	}
	return &Connection{conn, password}, nil
}
