# INSTRUCTION

Current image version:
  - Armbian 23.8.1 Bookworm
  - Homeassistant 2023.9.2
  1. Download Armbian 23.8.1 Bookworm image
  2. Connect SD card to PC
  3. Write image to SD card with Win32DiskImager
  4. Connect nano to router
  5. Run sd_prepare_v3.1.exe file
  6. Сhoose 2 for preparing sd card
  7. Enter ip address of nano
  8. Enter number of timezone (1 - Astana, 2 - Almaty, 3 - Shymkent, 4 - Tashkent, 5 - Atyrau, 6 - Oral, 7 - if you prepare for Slave Nano, 8 - for Old Nano)
  9. Enter version of Homeassistant (1 - old, 2 - new) and wait process of installation will finish
  10. Open nano via PuTTY. Type the command "sudo docker ps" that shows you all active docker containers, wait until docker homeassistant:landingpage changes to homeassistant:version (for example 2022.9.7)
  11. When version changes to numeric wait until uptime of Homeassistant docker will be more than 1 minutes then restart your nano with 'sudo shutdown -r now'
  12. After restart go to browser by typing 'IP_ADDRESS_OF_NANO'

- Account settings:
  1. Press “Create my smart home” and type account info (name: tablet, username: tablet, password: 88888888, confirm password: 88888888)
  2. Press "Geolocation" icon at the right side of input field (it must show your location with geolocation mark. If its not correct, move geolocation mark to correct one) and press "Next"
  3. In "help us help you" section do not check any option. Just press "Next"
  4. Press "Finish"
  5. (ONLY IF YOU CHOSE OLD VERSION OF HOMEASSISTANT) Reboot nano (sudo shutdown -r now). After reconnection via Putty.exe, wait untill uptime of Homeassistnat docker will be more that 1 minutes then type "sudo ./compare_hass_versions.sh" to update Homeassistant docker to version written in latest_version.txt file
  6. Press Settings → Add-ons → Add-on Store → search File Editor → Install (after installation activate options: Start on Boot, Watchdog, Show in sidebar). Change it's config in Configuration tab at the top (check "Directories First", uncheck "Enforce Basepath" and "Git", below "Git" delete .storage, press "Save")  and in "Info" tab press "Start" button
  7. Press Settings → Add-ons → Add-on Store (press "3 dots" icon at the right-top corner → Repositories → add https://github.com/zigbee2mqtt/hassio-zigbee2mqtt.git)
  8. Set Advanced Mode (press in left-bottom corner Tablet button and activate Advanced Mode)
  9. Add user (Settings → People → Users → Add User): Display name: admin, Username: admin, Password: 88889999 (check "Administrator" option)
  10. In integrations page (Settings → Devices & Services → Add Integration) (SKIP this for slave nano), search "Webrtc Camera", press 2 times "Submit" button without filling anything
  11. In integrations page (Settings → Devices & Services → Add Integration) (SKIP this for slave nano), search "Yandex Smart Home", choose sensor base_url, press "Submit" choose "Напрямую", press "Submit" → Finish
  12. In integrations page (Settings → Devices & Services → Add Integration), search "local ip address", add it
  13. In integrations page (Settings → Devices & Services → Add Integration) (SKIP this for slave nano), search "mqtt", in "broker" field type "172.30.32.1", username and password must be empty, add it. Then press on MQTT integration, then press "Configure" button, below MQTT settings press "Re-configure MQTT", press "next" at the bottom-right corner, uncheck "Enable Discovery" and press "Submit" at the bottom-right corner
  14. In integrations page (Settings → Devices & Services) delete "Google Cast" integration (if exists). To delete integration press on the integration you want to delete. Then press "3 dots" icon on the upper-right corner of the screen and press "Delete"
  15. In integrations page (Settings → Devices & Services) delete "Radio Browser" integration (if exists)
  16. In integrations page (Settings → Devices & Services) delete "Shopping list" integration (if exists)
  17. In integrations page (Settings → Devices & Services) delete "Bluetooth" integration (if exists)
  18. Delete all Areas (Settings → Areas & Zones). To delete area press on the area you want to delete, press "pencil" icon on the upper-right corner of the screen and press "Delete"
  19. Change via File Editor base_url.yaml file to specific url (optional)
  20. Open nano via PuTTY. Type "sudo armbian-config", check if Network → Wifi exists, if not then enter System → Firmware → update packages to activate Wifi
  21. Restart nano by typing command 'sudo shutdown -r now'
# END

For slave nano change base_url.yaml to "https://slave-nano", and comment all url field in ch/standart/rest_commands.yaml into "https://test", delete slave devices from poller.yaml

for execute to Windows:
  `GOOS=windows go build -o windows/sd_prepare.exe` 
