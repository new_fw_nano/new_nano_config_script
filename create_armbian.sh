#!/bin/bash
export FORCE=false
export TERM=xterm
# LOG_FILE="/install.log"
# ROOT_DIR="/mnt/armbian"
# mkdir -p "$ROOT_DIR"
INSTALL_DIR="/ch-installer"
mkdir -p "$INSTALL_DIR"
touch "$INSTALL_DIR"/rsync_root.log
touch "$INSTALL_DIR"/exclude.txt
if [ "$(cat /etc/os-release | grep "buster")" ]; then
	echo heartbeat > /sys/class/leds/LED1/trigger
elif [ "$(cat /etc/os-release | grep "bullseye")" ]; then
	echo heartbeat > /sys/class/leds/nanopi\:blue\:status/trigger
elif [ "$(cat /etc/os-release | grep "bookworm")" ]; then
	echo heartbeat > /sys/class/leds/nanopi\:blue\:status/trigger
fi
# mount -o loop,offset=4194304 ./*.img "$ROOT_DIR"
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root or sudo" 
  exit 1
fi

if [[ $1 == "force" || -f /ch-installer/force ]]; then
	export FORCE="true"
	rm -f /ch-installer/force
fi
# if [[ -f "${LOG_FILE}" ]]; then
# 	if [[ -f "${LOG_FILE}.1" ]]; then
# 		mv "${LOG_FILE}.1" "${LOG_FILE}.2"
# 	fi
# 	mv "${LOG_FILE}" "${LOG_FILE}.1"
# fi
# exec &> "${LOG_FILE}"
[[ -f /usr/lib/u-boot/platform_install.sh ]] && source /usr/lib/u-boot/platform_install.sh

echo 'libc6   libraries/restart-without-asking        boolean true' | debconf-set-selections
echo 'libc6:armhf     libraries/restart-without-asking        boolean true' | debconf-set-selections
echo 'libpam0g        libraries/restart-without-asking        boolean true' | debconf-set-selections
echo 'libpam0g:armhf  libraries/restart-without-asking        boolean true' | debconf-set-selections
echo 'libssl1.1       libraries/restart-without-asking        boolean true' | debconf-set-selections
echo 'libssl1.1:armhf libraries/restart-without-asking        boolean true' | debconf-set-selections


# install_rsyslog(){
#   cat > /etc/rsyslog.d/00-ch.conf <<EOF
#     *.warn  action(type="omfwd" target="52.232.75.196" port="514" protocol="udp" action.resumeRetryCount="100" queue.type="linkedList" queue.size="10000")
# EOF

# }

wait_addons()
{
	echo -n 'Waiting for addons start...'
	until [[ $(docker ps|grep -c 'addon') -gt 0 ]];	do
		sleep 15
		echo -n '.'
	done
	sleep 15
	echo -e '\naddons started'

}

stop_all()
{
	echo "Check if xl2tpd is running"
	used_space=$(df -h / | awk 'NR==2 {gsub(/G/,""); print $3}')
	threshold=4.9
	if (( $(echo "$used_space < $threshold" | bc -l) )); then
		echo "Used disk space is ok"
		if [ "$(sudo systemctl status xl2tpd | grep '('running')')" ] || [ "$(cat /usr/share/hassio/homeassistant/base_url.yaml | grep "slave")" ]; then
			echo "xl2tpd status is OK"
			echo "----------------------------------------------"
			echo "Stopping docker containers"
			sudo docker stop $(sudo docker ps -q)
			echo "Stopping docker"
			sudo systemctl stop docker
			sudo systemctl stop docker.socket
			echo "Stopping mbpoller, xl2tpd, avahi-daemon"
			sudo systemctl stop mbpoller
			sudo systemctl stop mbpollerwatch.path
			sudo systemctl stop mbpollerwatch.service
			sudo systemctl stop l2tp-watch.service
			sudo systemctl stop l2tpd.service
			sudo systemctl stop xl2tpd.service
			sudo systemctl stop avahi-daemon.service
			sudo systemctl disable avahi-daemon.socket
			sudo truncate -s 0 /var/lib/docker/containers/*/*-json.log
			# sudo sh -c "truncate -s 0 /var/lib/docker/containers/*/*-json.log"
			STATUS_D="$(systemctl is-active docker)"
			STATUS_M="$(systemctl is-active mbpoller)"
			STATUS_X="$(systemctl is-active xl2tpd)"
			if [[ "${STATUS_D}" == "inactive" ]] && [[ "${STATUS_M}" == "inactive" ]] && [[ "${STATUS_X}" == "inactive" ]]; then
				echo "----------------------------"
				echo "docker is inactive"
				echo "mbpoller is inactive"
				echo "xl2tpd.service is inactive"
				echo "----------------------------"
			else
				if [ "${STATUS_D}" != "inactive" ]; then
					echo "docker is not inactive"
				elif [ "${STATUS_M}" != "inactive" ]; then
					echo "mbpoller is not inactive"
				elif [ "${STATUS_X}" != "inactive" ]; then
					echo "xl2tpd is not inactive"
				fi
				exit 1
			fi
		else
			echo "-----------------------------------------------"
			echo "xl2tpd status is not working"
			echo "-----------------------------------------------"
			exit 1
		fi
	else
		echo "ERROR. Used disk space is more than 4.8, cannot start writing firmware"
		exit 1
	fi
}



# try to stop running services
stop_running_services()
{
	systemctl --state=running | awk -F" " '/.service/ {print $1}' | sort -r | \
	    grep -E -e "$1" | while read -r; do
		echo -e "\nStopping ${REPLY} \c"
		systemctl stop "${REPLY}" >/dev/null 2>&1 ||true
		while systemctl -q is-active "${REPLY}" >/dev/null 2>&1; do
			systemctl stop "${REPLY}" >/dev/null 2>&1 ||true
		done
	done
}


copy_rootfs()
{
	# stop running services
	echo -e "\nFiles currently open for writing:"
	lsof -w / | awk 'NR==1 || $4~/[0-9][uw]/' | grep -v "^COMMAND"
	echo -e "\nTrying to stop running services to minimize open files:\c"
  stop_all
	stop_running_services "nfs-|smbd|nmbd|winbind|ftpd|netatalk|monit|cron|webmin|rrdcached|syslog"
	stop_running_services "fail2ban|ramlog|folder2ram|postgres|mariadb|mysql|postfix|mail|nginx|apache|snmpd|dbus-"
	stop_running_services "baseurl-watch|l2tp-watch|xl2tpd|ch-wifi|mbpoller|mbpollerwatch|avahi-daemon|pppd"
	pkill dhclient 2>/dev/null
	pkill xl2tpd 2>/dev/null
	pkill pppd 2>/dev/null
	# stop_docker
	rsync -qavHXS --numeric-ids --bwlimit=6000 --exclude-from=$EX_LIST --log-file=/ch-installer/rsync_root.log / "${TempDir}"/rootfs || return $?
  sync	
}

# Create boot and root file system "$1" = boot, "$2" = root (Example: create_armbian "/dev/nand1" "/dev/sda3")
create_armbian()
{
  # create mount points, mount and clean

	umount_device "$emmccheck"
	format_emmc "$emmccheck"
  if [[ $eMMCFilesystemChoosen =~ ^(btrfs|f2fs)$ ]]; then
    [[ -n $1 ]] && mount ${1::-1}"1" "${TempDir}"/bootfs
		[[ -n $2 ]] && ( mount -o compress-force=zlib "$2" "${TempDir}"/rootfs 2> /dev/null || mount "$2" "${TempDir}"/rootfs )
	else
		[[ -n $2 ]] && ( mount -o compress-force=zlib "$2" "${TempDir}"/rootfs 2> /dev/null || mount "$2" "${TempDir}"/rootfs )
		[[ -n $1 && $1 != "spi" ]] && mount "$1" "${TempDir}"/bootfs
  fi

  rm -rf "${TempDir}"/bootfs/* "${TempDir}"/rootfs/*

  # sata root part
	# UUID=xxx...
	satauuid=$(blkid -o export "$2" | grep -w UUID)

	# SD card boot part -- wrong since more than one entry on various platforms
	# UUID=xxx...
	sduuid=$(blkid -o export /dev/mmcblk*p1 | grep -w UUID | grep -v "$root_partition_device")

  # write information to log
	# echo -e "\nOld UUID:  ${root_uuid}"
	# echo "SD UUID:   $sduuid" 
	# echo "SATA UUID: $satauuid"
	# echo "eMMC UUID: $emmcuuid $eMMCFilesystemChoosen"
	# echo "Boot: \$1 $1 $eMMCFilesystemChoosen"
	# echo "Root: \$2 $2 $eMMCFilesystemChoosen"

  # calculate usage and see if it fits on destination
	USAGE=$(df -BM | grep ^/dev | head -1 | awk '{print $3}' | tr -cd '[0-9]. \n')
	DEST=$(df -BM | grep ^/dev | grep /rootfs | awk '{print $4}' | tr -cd '[0-9]. \n')
	if [[ $USAGE -gt $DEST ]]; then
		echo "Partition too small. Needed: $USAGE MB Avaliable: $DEST MB"
		umount_device "$1"; umount_device "$2"
		return 3
	fi

  if [[ $1 == *nand* ]]; then
		# creating nand boot. Copy precompiled uboot
		rsync -aqc $BOOTLOADER/* "${TempDir}"/bootfs
	fi

  # write information to log
	# echo "Usage: $USAGE"
	# echo -e "Dest: $DEST\n\n/etc/fstab:"
	# cat /etc/fstab
	# echo -e "\n/etc/mtab:"
	# grep '^/dev/' /etc/mtab | grep -E -v "log2ram|folder2ram" | sort

  #echo "Counting files ... few seconds."
	#TODO=$(rsync -ahvrltDn --delete --stats --exclude-from=$EX_LIST / "${TempDir}"/rootfs | grep "Number of files:"|awk '{print $4}' | tr -d '.,')
	#echo -e "\nCopying ${TODO} files to $2. \c"
	#echo "Transferring rootfs to $2 ($USAGE MB). This will take approximately $(( $((USAGE/300)) * 1 )) minutes to finish. Please wait!"
	copy_retry=0
	until copy_rootfs
	do
		((copy_retry=copy_retry+1))
		if [[ $copy_retry -eq 4 ]]
		then
			return 1
		fi	
		echo "copy rootfs failed. Retry: $copy_retry"
	done
	echo "Cleaning up ... Almost done."

  # creating fstab from scratch
	rm -f "${TempDir}"/rootfs/etc/fstab
	mkdir -p "${TempDir}"/rootfs/etc "${TempDir}"/rootfs/media/mmcboot "${TempDir}"/rootfs/media/mmcroot

	# Restore TMP and swap
	echo "# <file system>					<mount point>	<type>	<options>							<dump>	<pass>" > "${TempDir}"/rootfs/etc/fstab
	echo "tmpfs						/tmp		tmpfs	defaults,nosuid							0	0" >> "${TempDir}"/rootfs/etc/fstab
	grep swap /etc/fstab >> "${TempDir}"/rootfs/etc/fstab

  # Boot from eMMC, root = eMMC or SATA / USB
	#
	if [[ $2 == ${emmccheck}p* || $1 == ${emmccheck}p* ]]; then
    if [[ $2 == "${DISK_ROOT_PART}" ]]; then
			local targetuuid=$satauuid
			local choosen_fs=$eMMCFilesystemChoosen
			echo "Finalizing: boot from eMMC, rootfs on USB/SATA/NVMe." 
			if [[ $eMMCFilesystemChoosen =~ ^(btrfs|f2fs)$ ]]; then
				echo "$emmcuuid	/media/mmcroot  $eMMCFilesystemChoosen	${mountopts[$eMMCFilesystemChoosen]}" >> "${TempDir}"/rootfs/etc/fstab
			fi
		else
			local targetuuid=$emmcuuid
			local choosen_fs=$eMMCFilesystemChoosen
			echo "Finishing full install to eMMC." 
		fi

    # fix that we can have one exlude file
		cp -R /boot "${TempDir}"/bootfs

    # old boot scripts
		sed -e 's,root='"$root_uuid"',root='"$targetuuid"',g' -i "${TempDir}"/bootfs/boot/boot.cmd

    # new boot scripts
		if [[ -f "${TempDir}"/bootfs/boot/armbianEnv.txt ]]; then
			sed -e 's,rootdev=.*,rootdev='"$targetuuid"',g' -i "${TempDir}"/bootfs/boot/armbianEnv.txt
		else
      sed -e 's,setenv rootdev.*,setenv rootdev '"$targetuuid"',g' -i "${TempDir}"/bootfs/boot/boot.cmd
			[[ -f "${TempDir}"/bootfs/boot/boot.ini ]] && sed -e 's,^setenv rootdev.*$,setenv rootdev "'"$targetuuid"'",' -i "${TempDir}"/bootfs/boot/boot.ini
			[[ -f "${TempDir}"/rootfs/boot/boot.ini ]] && sed -e 's,^setenv rootdev.*$,setenv rootdev "'"$targetuuid"'",' -i "${TempDir}"/rootfs/boot/boot.ini
		fi

    mkimage -C none -A arm -T script -d "${TempDir}"/bootfs/boot/boot.cmd "${TempDir}"/bootfs/boot/boot.scr	>/dev/null 2>&1 || (echo 'Error while creating U-Boot loader image with mkimage' >&2 ; exit 5)

		# fstab adj
		if [[ "$1" != "$2" ]]; then
			echo "$emmcbootuuid	/media/mmcboot	ext4    ${mountopts[ext4]}" >> "${TempDir}"/rootfs/etc/fstab
			echo "/media/mmcboot/boot   				/boot		none	bind								0       0" >> "${TempDir}"/rootfs/etc/fstab
		fi

    # if the rootfstype is not defined as cmdline argument on armbianEnv.txt
		if ! grep -qE '^rootfstype=.*' "${TempDir}"/bootfs/boot/armbianEnv.txt; then
			# Add the line of type of the selected rootfstype to the file armbianEnv.txt
			echo "rootfstype=$choosen_fs" >> "${TempDir}"/bootfs/boot/armbianEnv.txt
		fi

    if [[ $eMMCFilesystemChoosen =~ ^(btrfs|f2fs)$ ]]; then
			echo "$targetuuid	/		$choosen_fs	${mountopts[$choosen_fs]}" >> "${TempDir}"/rootfs/etc/fstab
			# swap file not supported under btrfs but we might have made a partition
			[[ -n ${emmcswapuuid} ]] && sed -e 's,/var/swap.*,'$emmcswapuuid' 	none		swap	sw								0	0,g' -i "${TempDir}"/rootfs/etc/fstab
			sed -e 's,rootfstype=.*,rootfstype='$eMMCFilesystemChoosen',g' -i "${TempDir}"/bootfs/boot/armbianEnv.txt
		else
			sed -e 's,rootfstype=.*,rootfstype='$choosen_fs',g' -i "${TempDir}"/bootfs/boot/armbianEnv.txt
			echo "$targetuuid	/		$choosen_fs	${mountopts[$choosen_fs]}" >> "${TempDir}"/rootfs/etc/fstab
		fi

    if [[ $(type -t write_uboot_platform) != function ]]; then
			echo "Error: no u-boot package found, exiting"
			return 6
		fi
		write_uboot_platform "$DIR" "$emmccheck"

	fi

} # create_armbian

# Accept device as parameter: for example /dev/sda unmounts all their mounts
umount_device()
{
	if [[ -n $1 ]]; then
		device="$1";
		for n in ${device}*; do
			echo "umount device:$n"
			if [[ $device != "$n" ]]; then
				while mount|grep -q "$n"; do
					umount -l "$n" >/dev/null 2>&1
				done
			fi
		done
	fi
} # umount_device

# formatting eMMC [device] example /dev/mmcblk1 - one can select filesystem type
format_emmc()
{
  eMMCFilesystemChoosen="ext4"
  # deletes all partitions on eMMC drive
	dd bs=1 seek=446 count=64 if=/dev/zero of="$1" >/dev/null 2>&1
	# calculate capacity and reserve some unused space to ease cloning of the installation
	# to other media 'of the same size' (one sector less and cloning will fail)
	QUOTED_DEVICE=$(echo "$1" | sed 's:/:\\\/:g')
	CAPACITY=$(parted "$1" unit s print -sm | awk -F":" "/^${QUOTED_DEVICE}/ {printf (\"%0d\", \$2 / ( 1024 / \$4 ))}")

  if [[ $CAPACITY -lt 4000000 ]]; then
		# Leave 2 percent unpartitioned when eMMC size is less than 4GB (unlikely)
		LASTSECTOR=$(( 32 * $(parted "$1" unit s print -sm | awk -F":" "/^${QUOTED_DEVICE}/ {printf (\"%0d\", ( \$2 * 98 / 3200))}") -1 ))
	else
		# Leave 1 percent unpartitioned
		LASTSECTOR=$(( 32 * $(parted "$1" unit s print -sm | awk -F":" "/^${QUOTED_DEVICE}/ {printf (\"%0d\", ( \$2 * 99 / 3200))}") -1 ))
	fi

  parted -s "$1" -- mklabel msdos
	echo "Formating $1 to $eMMCFilesystemChoosen ... please wait."
	# we can't boot from btrfs or f2fs
  if [[ $eMMCFilesystemChoosen =~ ^(btrfs|f2fs)$ ]]; then
		local partedFsType="${eMMCFilesystemChoosen}"
		if [[ $eMMCFilesystemChoosen == "f2fs" ]]; then
			partedFsType=''
		fi
    # check whether swap is currently defined and a new swap partition is needed
		grep -q swap /etc/fstab
    case $? in
			0)
				parted -s "$1" -- mkpart primary "$partedFsType" "${FIRSTSECTOR}s" $(( FIRSTSECTOR + 131071 ))s
				parted -s "$1" -- mkpart primary "$partedFsType" $(( FIRSTSECTOR + 131072 ))s $(( FIRSTSECTOR + 393215 ))s
				parted -s "$1" -- mkpart primary "$partedFsType" $(( FIRSTSECTOR + 393216 ))s "${LASTSECTOR}s"
				partprobe "$1"
				mkfs.ext4 "${mkopts[ext4]}" "$1"'p1' 
				mkswap "$1"'p2'
				mkfs.${eMMCFilesystemChoosen} "$1"'p3' "${mkopts[$eMMCFilesystemChoosen]}"
				emmcbootuuid=$(blkid -o export "$1"'p1' | grep -w UUID)
				emmcswapuuid=$(blkid -o export "$1"'p2' | grep -w UUID)
				emmcuuid=$(blkid -o export "$1"'p3' | grep -w UUID)
				dest_root=$emmccheck'p3'
				;;
			*)
				parted -s "$1" -- mkpart primary "$partedFsType" "${FIRSTSECTOR}s" $(( FIRSTSECTOR + 131071 ))s
				parted -s "$1" -- mkpart primary "$partedFsType" $(( FIRSTSECTOR + 131072 ))s ${LASTSECTOR}s
				partprobe "$1"
				mkfs.ext4 "${mkopts[ext4]}" "$1"'p1' 
				mkfs.${eMMCFilesystemChoosen} "$1"'p2' "${mkopts[$eMMCFilesystemChoosen]}"
				emmcbootuuid=$(blkid -o export "$1"'p1' | grep -w UUID)
				emmcuuid=$(blkid -o export "$1"'p2' | grep -w UUID)
				dest_root=$emmccheck'p2'
				;;
		esac
  else
		parted -s "$1" -- mkpart primary $eMMCFilesystemChoosen ${FIRSTSECTOR}s ${LASTSECTOR}s
		partprobe "$1"
		mkfs.${eMMCFilesystemChoosen} "${mkopts[$eMMCFilesystemChoosen]}" "$1"'p1'
		emmcuuid=$(blkid -o export "$1"'p1' | grep -w UUID)
		emmcbootuuid=$emmcuuid
	fi

}

# script configuration

EX_LIST="/ch-installer/exclude.txt"
touch EX_LIST
cp "/usr/lib/nand-sata-install/exclude.txt" "${EX_LIST}"
[ -f /etc/default/openmediavault ] && echo '/srv/*' >> "${EX_LIST}"
{ 
	echo '/var/log/*' 
	echo '/ch-installer/*'
} >> "${EX_LIST}"

# read in board info
[[ -f /etc/armbian-release ]] && source /etc/armbian-release



# exceptions
if grep -q 'sun4i' /proc/cpuinfo; then DEVICE_TYPE="a10";
elif grep -q 'sun5i' /proc/cpuinfo; then DEVICE_TYPE="a13";
else DEVICE_TYPE="a20"; fi
BOOTLOADER="${CWD}/${DEVICE_TYPE}/bootloader"
case ${LINUXFAMILY} in
	rk3328|rk3399|rockchip64)
		FIRSTSECTOR=32768
		;;
	*)
		FIRSTSECTOR=8192
		;;
esac


#recognize_root
root_uuid=$(sed -e 's/^.*root=//' -e 's/ .*$//' < /proc/cmdline)
root_partition=$(blkid | tr -d '":' | grep "${root_uuid}" | awk '{print $1}')
root_partition_device="${root_partition::-2}"

# find targets: NAND, EMMC, SATA, SPI flash, NVMe
emmccheck=$(ls -d -1 /dev/mmcblk* | grep -w 'mmcblk[0-9]' | grep -v "$root_partition_device");

# define makefs and mount options
declare -A mkopts mountopts
# for ARMv7 remove 64bit feature from default mke2fs format features
if [[ $LINUXFAMILY == mvebu ]]; then
	mkopts[ext2]='-O ^64bit -qF'
	mkopts[ext3]='-O ^64bit -qF'
	mkopts[ext4]='-O ^64bit -qF'
else
	mkopts[ext2]='-qF'
	mkopts[ext3]='-qF'
	mkopts[ext4]='-qF'
fi

mkopts[btrfs]='-f'
mkopts[f2fs]=''

mountopts[ext2]='defaults,noatime,nodiratime,commit=600,errors=remount-ro,x-gvfs-hide	0	1'
mountopts[ext3]='defaults,noatime,nodiratime,commit=600,errors=remount-ro,x-gvfs-hide	0	1'
mountopts[ext4]='defaults,noatime,nodiratime,commit=600,errors=remount-ro,x-gvfs-hide	0	1'
mountopts[btrfs]='defaults,noatime,nodiratime,commit=600,compress=lzo,x-gvfs-hide			0	2'
mountopts[f2fs]='defaults,noatime,nodiratime,x-gvfs-hide	0	2'

TempDir="/mnt"
sync &&	mkdir -p "${TempDir}"/bootfs "${TempDir}"/rootfs


if [[ -n $emmccheck ]]; then
	dest_boot=$emmccheck'p1'
	dest_root=$emmccheck'p1'
	if [[ $FORCE != "true" ]]; then
		mount -t ext4 "$dest_root" "$TempDir/rootfs" || true
		if [[ ( -e ${TempDir}/rootfs/etc/ch-release || -e ${TempDir}/rootfs/etc/armbian-release ) ]]; then
			if [ "$(cat /etc/os-release | grep "buster")" ]; then
				echo default-on > /sys/class/leds/LED1/trigger
			elif [ "$(cat /etc/os-release | grep "bullseye")" ]; then
				echo default-on > /sys/class/leds/nanopi\:blue\:status/trigger
			elif [ "$(cat /etc/os-release | grep "bookworm")" ]; then
				echo default-on > /sys/class/leds/nanopi\:blue\:status/trigger
			fi
			echo "$(tput setaf 1)Installation canceled. Connected home is already installed on this device. To force installation run 'touch /ch-installer/force && reboot' $(tput sgr 0)"
			exit 1
		fi
		umount "$TempDir/rootfs"
	fi
else
    echo "$(tput setaf 1)eMMC check failed. Exiting ...$(tput sgr 0)"
    exit 1
fi

echo "Flashing to $dest_root"
RETRY=3
until create_armbian "$dest_boot" "$dest_root"
do
	echo "Retrying flash filesystem..."
	((RETRY=RETRY-1))
	if [ $RETRY -eq 0 ]; then
		if [ "$(cat /etc/os-release | grep "buster")" ]; then
			echo default-on > /sys/class/leds/LED1/trigger
		elif [ "$(cat /etc/os-release | grep "bullseye")" ]; then
			echo default-on > /sys/class/leds/nanopi\:blue\:status/trigger
		elif [ "$(cat /etc/os-release | grep "bookworm")" ]; then
			echo default-on > /sys/class/leds/nanopi\:blue\:status/trigger
		fi
		echo "Failed to flash $dest_root"
		exit 1
	fi
done

if [ "$(cat /etc/os-release | grep "buster")" ]; then
	echo none > /sys/class/leds/LED1/trigger
	echo none > /sys/class/leds/LED2/trigger
elif [ "$(cat /etc/os-release | grep "bullseye")" ]; then
	echo none > /sys/class/leds/nanopi\:blue\:status/trigger
	echo none > /sys/class/leds/green\:wan/trigger
elif [ "$(cat /etc/os-release | grep "bookworm")" ]; then
	echo none > /sys/class/leds/nanopi\:blue\:status/trigger
	echo none > /sys/class/leds/green\:wan/trigger
fi

halt


# DISK_ROOT_PART, DIR ?
